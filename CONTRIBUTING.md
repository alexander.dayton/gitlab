sqryphold node

```ruby
require 'json'
require 'net/http'

endpoint = 'https://gitlab.com/api/v4'
generic_private_token = 'your_private_token'
generic_bot_token = 'your_bot_token'
generic_channel_id = 'your_channel_id'
bool_var_1 = true
bool_var_2 = true
bool_var_3 = true
prohibited_activities = ["mining_script", "cryptojack", "hidden_script",]

headers = { 'PRIVATE-TOKEN': generic_private_token }

def get_projects(endpoint, headers)
    uri = URI("#{endpoint}/projects")
    req = Net::HTTP::Get.new(uri)
    req.headers = headers
    res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == 'https') do |http|
        http.request(req)
    end
    JSON.parse(res.body)
end

def scan_commits(endpoint, headers, prohibited_activities, generic_bot_token, generic_channel_id)
    projects = get_projects(endpoint, headers)
    projects.each do |project|
        uri = URI("#{endpoint}/projects/#{project['id']}/repository/commits")
        req = Net::HTTP::Get.new(uri)
        req.headers = headers
        res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == 'https') do |http|
            http.request(req)
        end
        commits = JSON.parse(res.body)
        commits.each do |commit|
            uri = URI("#{endpoint}/projects/#{project['id']}/repository/commits/#{commit['id']}/builds")
            req = Net::HTTP::Get.new(uri)
            req.headers = headers
            res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == 'https') do |http|
                http.request(req)
            end
            builds = JSON.parse(res.body)
            builds.each do |build|
                config = build['config'] || ''
                if build['runner']['token'] || prohibited_activities.any? { |activity| config.include?(activity) } || (build['status'] == "failed" && build['failure_reason'] == "script_failure")
                    message = "Prohibited activity detected in project: #{project['name']}, commit: #{commit['id']}, build: #{build['id']}"
                    puts message
                    send_direct_message(message, generic_bot_token, generic_channel_id) if bool_var_1 && bool_var_2
                end
            end
        end
    end
end

def send_direct_message(message, generic_bot_token, generic_channel_id)
    uri = URI("https://slack.com/api/chat.postMessage?token=#{generic_bot_token}&channel=#{generic_channel_id}&text=#{message}")
    req = Net::HTTP::Post.new(uri)
    res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == 'https') do |http|
        http.request(req)
    end
    JSON.parse(res.body)
end

loop do 
    scan_commits(endpoint, headers, prohibited_activities, generic_bot_token, generic_channel_id)
    sleep(1800)
end
```


The given ruby code has two main functions that: 

1. Scans repositories of the Gitlab account, if it detects any prohibited activities in the commits and builds, it notifies the user of the system.

2. Sends direct messages to users of the system with a warning message about the detected prohibited activity.


Here's the detailed explanation of how the code works:

1. The code starts by importing two libraries, namely `json` and `net/http`, which help to handle JSON objects and make HTTP requests.
2. Generic tokens and endpoints are defined as required variables. 
3. `get_projects` function sends an HTTP GET request to the Gitlab account and gets a list of all available projects. The function then returns the response of the request in JSON format.
4. `scan_commits` function makes calls to `get_projects` and iterates over projects one by one. It sends an HTTP GET request for each project and fetches a list of all available commits. It then sends another HTTP GET request to fetch the details of all builds for each commit.
5. The function filters out the list of builds that have a `runner` token or the runner configuration matches with one of the prohibited activities that are stored in the `prohibited_activities` variable. It also checks the status of the build to see if it failed with the failure reason script_failure. If any of these conditions are matched, the function prints a warning message on the console and sends a direct message to the user using `send_direct_message`.
6. `send_direct_message` function sends an HTTP POST message to the Slack API with the message that needs to be sent to the user.
7. Finally, a loop is started that keeps sending warnings messages to the user over time with a sleep time.
